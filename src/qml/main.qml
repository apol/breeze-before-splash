/*
 *   Copyright 2017 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.4
import QtQuick.Window 2.2

Item
{
     Repeater {
        model: Qt.application.screens
        delegate: Item {
            Window {
                id: window
                View {
                    anchors.fill: parent
                }

                Component.onCompleted: {
                    window.screen = modelData
                    x = window.screen.virtualX
                    y = window.screen.virtualY
                    showFullScreen()
                }
            }
        }
    }
}
