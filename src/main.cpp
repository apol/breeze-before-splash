#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

// Qt.application.screens was introduced in 5.9.0
#if QT_VERSION < QT_VERSION_CHECK(5,9,0)
    const QUrl source(QStringLiteral("qrc:/qml/View.qml"));
    foreach (QScreen* screen, app.screens()) {
        QQuickView* view = new QQuickView();
        view->setResizeMode(QQuickView::SizeRootObjectToView);
        view->setScreen(screen);
        view->setSource(source);
        view->showFullScreen();
    }

#else
    const QUrl source(QStringLiteral("qrc:/qml/main.qml"));
    QQmlApplicationEngine engine(source);
#endif
    return app.exec();
}
